const express = require('express');
const app = express();
const http = require('http').Server(app);
const path = require('path');
const favicon = require('serve-favicon');
const socketio = require('socket.io')(http);
const cp = require('child_process');


app.use(express.static(path.join(__dirname, 'static')));
app.use(favicon(path.join(__dirname, 'static', 'images', 'favicon.ico')));

http.listen(process.env.PORT || 5000, () => {
  console.log(`Server running at 127.0.0.1:${process.env.port || 5000}`);
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'))
});

socketio.on('connection', (socket) => {
  socket.on('message', (msg) => {
    if (msg.startsWith('/date')) {
  const args = msg.split(' '); // Sépare la commande et ses arguments
  const command = args.shift(); // Retire la première partie (la commande)

  cp.execFile(command, args, (error, stdout, stderr) => {
    if (error) {
      console.error(`Erreur d'exécution de la commande : ${error}`);
      return;
    }
    console.log(`Sortie de la commande : ${stdout}`);
    socketio.emit('message', stdout);
  });
} else {
  socketio.emit('message', msg);
};

};
};

